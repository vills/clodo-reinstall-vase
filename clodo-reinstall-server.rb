#!/usr/bin/env ruby

# == AUTH PARAMS =====================================
api_u = "username"							# username 
api_k = "38f290703f14cs911a3158d24c065804"	# api key



# == SCRIPT CODE == DON'T CHANGE BELOW ===============

require 'rubygems'
require 'cloudservers/cloudservers'
require 'timeout'

# parsing params
server_num = ARGV[0].to_i
raise "First param need to be server num." if server_num.to_i < 1

# trying to authenticate
begin
	auth_link = server_num < 10000 ? CloudServers::AUTH_CLODO : CloudServers::AUTH_CLODO_KH
	api = CloudServers::Connection.new(:username => api_u, :api_key => api_k, :auth_url => auth_link)
rescue
	abort "ERROR: Can't get authorization"
end


# have we this server?
begin
	server = api.get_server(server_num)
rescue
	abort "ERROR: Can't find server with ID #{server_num}"
end

# to be or not to be ?
puts "Server: #{server.name}"
print "Reinstall? [y/n] "
if STDIN.gets.strip.capitalize == 'Y'
	begin
		server.rebuild!
	rescue
		abort "ERROR: Can't reinstall server right now"
	end
	
	# installing ..
	#sleep(5) # sleeping before check status
	server_now_status = ''
	while true
		begin
			timeout(7) do
				server.populate

				if server_now_status != server.status
					server_now_status = server.status
					puts ""
					case server_now_status
						when "is_request" then print "\tRequesting "
						when "is_install" then print "\tInstalling "
					end
				end

			end
		rescue
			nil
		end

		break if server_now_status == "is_running"
		print "."
		STDOUT.flush
		sleep(3)
	end

	puts "\nServer #{server.name} reinstalled!"
else
	puts "ok, exiting.."	
end
