Скрипт переустановки операционной системы на облачном сервере Clodo
===================================================================

Нужна поддержка JSON'а:

> `gem install json`


В начале файла clodo-reinstall-server.rb нужно прописать свои логин доступа к панели управления Clodo и ключ доступа к API Clodo.ru

> `api_u = "my_login"`

> `api_k = "38f290703f14cs911a3158d24c065804"`


Потом добавить бит исполняемости:

>	`chmod u+x clodo-reinstall-server.rb`


Скрипт принимает только один параметр - номер сервера:

>	`./clodo-reinstall-server.rb 73`